//1. What directive is used by Node.js in loading the modules it needs?
Answer: require() Directive

//2. What Node.js module contains a method for server creation?
Answer: http module

// 3.What is the method of the http object responsible for creating a server using node.js
Answer: http.createServer()

// 4. what method of the response object allows us to set status code and content types
Answer: response.writeHead()

// 5. Where will console.log() output its contents when run in Node.js?
Answer: in the terminal

// 6 What property of the requst object contains the address's endpoint?

Answer: Routes