// No. 5
let http = require("http")


// No. 6
const port = 3000


// No. 7
const server = http.createServer((req, res) => {
	//  No.9
	if(req.url == '/login'){ 

		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Welcome to the login page.')
	}

	else{ 
		
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end('Page not found')
	}

})

// No. 7
server.listen(port);

// No. 8
console.log(`Server now accessible at localhosts: ${port}`)

